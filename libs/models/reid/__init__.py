import cv2
import numpy as np
import tensorflow as tf
import os
from libs.models.reid.image_part_aligned import Model
from libs.utils.net_utils import load_pretrained_
from libs.utils.log import logger
from libs.utils import bbox_utils
import libs.utils.cython_utils
from libs.utils.common import check

# def load_reid_model():
#     img = tf.placeholder(tf.float32, [4, 64, 64, 3])
#     model = Model(img)

# @staticmethod
def im_preprocess(image):
    image = np.asarray(image, np.float32)
    image -= np.array([104, 117, 123], dtype=np.float32).reshape(1, 1, -1)
    # image = image.transpose((2, 0, 1))
    return image

# @staticmethod
def extract_image_patches(image, bboxes):
    # print(image, bboxes, 'bugs here!!')
    bboxes = np.round(bboxes).astype(np.int)
    bboxes = bbox_utils.clip_boxes(bboxes, image.shape)
    patches = [image[box[1]:box[3], box[0]:box[2]] for box in bboxes]
    # print(patches, '111111111111')
    return patches

class ReIdModel():
    def __init__(self, scope, sess):
        self.scope = scope
        self.sess = sess
        # self.input = tf.placeholder(tf.float32, [None, 64, 64, 3])
        self.model = Model
        self.build_model()

    def build_model(self):
        with tf.name_scope(self.scope):
            with tf.variable_scope(self.scope):
                self.input = tf.placeholder(tf.float32, [None, 640, 1152, 3])
                self.inp_size = tf.shape(self.input)
                self.feature = self.model(self.input)
            self.get_pretrained_variables()
        self.load_pretrained()

    def get_pretrained_variables(self):
        self.vars = []
        for var in tf.trainable_variables(scope=self.scope):
            # print(var)
            if check(var.name):
                self.vars.append(var)

    def load_pretrained(self, ckpt = 'storage/nets/googlenet_part8_all_xavier_ckpt_56.h5'):
        load_pretrained_(self.sess, self.vars, ckpt)
        logger.info('Load ReID model from {}'.format(ckpt))


    def extract_reid_features(self, image, tlbrs):
        # print(tlbrs, 'test------------------')
        if len(tlbrs) == 0:
            return None

        patches = extract_image_patches(image, tlbrs)
        # print(type(patches))
        # for p in patches:
        #     print(cv2.resize(p, (640, 1152)).shape)
        patches = np.asarray([im_preprocess(cv2.resize(p, (1152, 640))) for p in patches], dtype=np.float32)

        # im_var = tf.convert_to_tensor(patches)
        features = self.sess.run(self.feature, feed_dict={self.input: patches})
        return features


if __name__ == '__main__':
    tf.reset_default_graph()
    x = np.random.randint(0, 4, (1, 640, 1152, 3))
    sess = tf.Session()
    model = ReIdModel('scope', sess)
    features = sess.run(model.feature, feed_dict={model.input: x})
    print(features)