import tensorflow as tf
import tensorflow.contrib.slim as slim
from libs.models.backbone.googlenet import GoogLeNet
import numpy as np

def Model(inputs, n_parts=8):
    feature = GoogLeNet(inputs)
    feature = slim.conv2d(feature, 512, kernel_size=[1, 1], scope='conv_input_feat')

    conv_att = slim.conv2d(feature, n_parts, kernel_size=[1, 1], scope='conv_att')
    att_weight = tf.math.sigmoid(conv_att)
    # print(tf.expand_dims(att_weight[..., 0], -1))

    linear_features = []
    feats = [64]*8
    for i in range(n_parts):
        masked_feature = feature * tf.expand_dims(att_weight[..., i], -1)
        # print(masked_feature).value_index()[1:3])
        pooled_feature = slim.avg_pool2d(masked_feature, masked_feature.get_shape().as_list()[1:3])
        shape = pooled_feature.get_shape().as_list()
        # print(pooled_feature, shape, 'bugs !!!')
        # print(shape[0])
        reshape_pooled_feature = tf.reshape(pooled_feature, (-1, shape[-1]))
        linear_features.append(slim.fully_connected(reshape_pooled_feature, feats[i], scope='linear_feature{}'.format(i+1)))

    concat_features = tf.concat(linear_features, -1)
    normed_feature = concat_features / tf.clip_by_value(tf.norm(concat_features, axis=-1, keepdims=True),
                                                        clip_value_min=1e-6, clip_value_max=np.inf)

    return normed_feature
