import tensorflow as tf 
import tensorflow.contrib.slim as slim
trunc_normal = lambda stddev: tf.truncated_normal_initializer(0.0, stddev)

n_feats = [64, 128, 256, 512]
def squeeze_base(inputs, is_training=False, pretrained=True):
    with tf.variable_scope('feature_extractor', [inputs]):
        with slim.arg_scope([slim.conv2d, slim.fully_connected],
                                    weights_initializer=trunc_normal(0.01)):
            with slim.arg_scope([slim.conv2d, slim.max_pool2d],
                          stride=1, padding='SAME'):

                net = slim.conv2d(inputs, 64, [3, 3], stride=2, scope='conv1.0') # have relu 0 1
                net1 = net
                net = slim.max_pool2d(net, [3, 3], stride=2, scope='maxpool2') # maxpool2
                #fire ==============3====================
                net = slim.conv2d(net, 16, [1, 1], scope='conv2.1.squeeze') #squeeze
                net1x1 = slim.conv2d(net, 64, [1, 1], scope='conv2.1.expand1x1') #expand1x1
                net3x3 = slim.conv2d(net, 64, [3, 3], scope='conv2.1.expand3x3') #expand3x3
                net = tf.concat([net1x1, net3x3], -1)
                #fire ==============4====================
                net = slim.conv2d(net, 16, [1, 1], scope='conv2.2.squeeze')
                net1x1 = slim.conv2d(net, 64, [1, 1], scope='conv2.2.expand1x1')
                net3x3 = slim.conv2d(net, 64, [3, 3], scope='conv2.2.expand3x3')
                net = tf.concat([net1x1, net3x3], -1)
                net2 = net

                net = slim.max_pool2d(net, [3, 3], stride=2, scope='maxpool5') # max 5
                #fire ==============6====================
                net = slim.conv2d(net, 32, [1, 1], scope='conv3.1.squeeze')
                net1x1 = slim.conv2d(net, 128, [1, 1], scope='conv3.1.expand1x1')
                net3x3 = slim.conv2d(net, 128, [3, 3], scope='conv3.1.expand3x3')
                net = tf.concat([net1x1, net3x3], -1)
                #fire ==============7====================
                net = slim.conv2d(net, 32, [1, 1], scope='conv3.2.squeeze')
                net1x1 = slim.conv2d(net, 128, [1, 1], scope='conv3.2.expand1x1')
                net3x3 = slim.conv2d(net, 128, [3, 3], scope='conv3.2.expand3x3')
                net = tf.concat([net1x1, net3x3], -1)
                net3 = net

                net = slim.max_pool2d(net, [3, 3], stride=2, scope='maxpool8') # max 8
                #fire ==============9====================
                net = slim.conv2d(net, 48, [1, 1], scope='conv4.1.squeeze')
                net1x1 = slim.conv2d(net, 192, [1, 1], scope='conv4.1.expand1x1')
                net3x3 = slim.conv2d(net, 192, [3, 3], scope='conv4.1.expand3x3')
                net = tf.concat([net1x1, net3x3], -1)
                #fire ==============10====================
                net = slim.conv2d(net, 48, [1, 1], scope='conv4.2.squeeze')
                net1x1 = slim.conv2d(net, 192, [1, 1], scope='conv4.2.expand1x1')
                net3x3 = slim.conv2d(net, 192, [3, 3], scope='conv4.2.expand3x3')
                net = tf.concat([net1x1, net3x3], -1)
                #fire ==============11====================
                net = slim.conv2d(net, 64, [1, 1], scope='conv4.3.squeeze')
                net1x1 = slim.conv2d(net, 256, [1, 1], scope='conv4.3.expand1x1')
                net3x3 = slim.conv2d(net, 256, [3, 3], scope='conv4.3.expand3x3')
                net = tf.concat([net1x1, net3x3], -1)
                #fire ==============12====================
                net = slim.conv2d(net, 64, [1, 1], scope='conv4.4.squeeze')
                net1x1 = slim.conv2d(net, 256, [1, 1], scope='conv4.4.expand1x1')
                net3x3 = slim.conv2d(net, 256, [3, 3], scope='conv4.4.expand3x3')
                net = tf.concat([net1x1, net3x3], -1)
                net4 = net

                return [net1, net2, net3, net4]


def squeeze_head(inputs, is_training=False):
    with slim.arg_scope([slim.conv2d, slim.max_pool2d],
                        stride=1, padding='SAME'):
        net = slim.dropout(inputs)
        net = slim.conv2d(net, 256, [3, 3], scope='stage_0.1')
        return net


