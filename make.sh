#!/usr/bin/env bash

echo "build solvers..."
cd libs/utils/cython_utils
python setup.py build_ext --inplace

cd nms
python setup.py build_ext --inplace
cd /content/tf_MOTDT
